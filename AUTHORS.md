# Project Authors

## Code
* [Asaf Bartov](https://meta.wikimedia.org/wiki/User:Ijon)

## Interface translations

* [User:Ата](https://meta.wikimedia.org/wiki/User:Ата) (Ukrainian)
* [User:MariaCurista](https://meta.wikimedia.org/wiki/User:MariaCurista) (Portuguese)
* [User:Amire80](https://meta.wikimedia.org/wiki/User:Amire80) (Hebrew)

