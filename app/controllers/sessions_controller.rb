class SessionsController < ApplicationController
  def new
    render :new
  end

  def create
    if params[:code].present? # got initial OAuth2 authorization code, can proceed to request user access token
      begin
        client = OAuth2::Client.new(Rails.configuration.constants.oauth_consumer_token, Rails.configuration.constants.oauth_consumer_secret, site: "https://meta.wikimedia.org/w/rest.php", authorize_url: 'oauth2/authorize', token_url: 'oauth2/access_token' , logger: Logger.new('oauth2.log', 'weekly'))
        @access_token = client.auth_code.get_token(params[:code], redirect_uri: Rails.configuration.constants.oauth_callback_url, client_id: Rails.configuration.constants.oauth_consumer_token, client_secret: Rails.configuration.constants.oauth_consumer_secret )
        @got_token = true
        reset_session
        session[:access_token] = @access_token.token
        flash[:notice] = t(:oauth_success)
        #Rails.logger.info "Got access token: #{@access_token.token}"
      rescue
        @got_token = false
        Rails.logger.error "Failed to get access token: #{$!}"
        flash[:error] = t(:oauth_failed, msg: $!)
      end
    else
      Rails.logger.error "No authorization code received"
      flash[:error] = t(:bad_login)
    end
    redirect_to '/'
  end
end